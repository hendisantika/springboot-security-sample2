# springboot-security-demo2

springboot-security-demo2

- Spring Boot 2.0.6 & Spring Security 5
- User Join, Login
- Use simple(single) Role

NEXT
- User authentication management
- Method level security
  - @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
  - @PreAuthorize("hasRole('ADMIN')"), @Secured({"ROLE_USER", "ROLE_ADMIN"})
  
## Reference

- http://spring.io/guides/gs/securing-web/
- http://spring.io/guides/topicals/spring-security-architecture/
- https://docs.spring.io/spring-security/site/docs/current/guides/html5/helloworld-boot.html

## Quick Start
Pre Installed
- JDK 1.8 (or Java 10)
- Maven 3.5
- Git

Run
```
git clone https://gitlab.com/hendisantika/springboot-security-sample2.git
cd springboot-security-sample2
mvn clean spring-boot:run
```

## Test

http://localhost:8080

- id = admin
- pw = 123456

http://localhost:8080/h2-console

### Tested
- STS (Eclipse) 3.8.4
- IntelliJ IDEA 2018.2

```
//@formatter:off & //@formatter:on
eclipse : Preferences -> Java -> Code style -> Formatter -> Edit... (or New...) > Off/On Tags
intelliJ : Preferences -> Editor -> Code Style > Formatter Control > Enable formatter markers in comments
```


## Dependency

### Spring Boot 2.0.4.RELEASE
- spring-boot-starter-web
- spring-boot-starter-security

#### Environment
- Java version: 8 Update 172 or 10.0.1
- Spring Boot version: 2.0.6
- Maven version: 3.5.2
- Lombok version: 1.18.0
- Default Encoding: UTF-8
- Default SCM : git

## Screenshots

Login Page

![Login Page](img/login.png "Login Page")

No Login Page

![No Login Page](img/no.png "No Login Page")

Join Login Page

![Join Login Page](img/join.png "Join Login Page")

Admin 1 Page

![Admin 1 Page](img/admin.png "Admin 1 Page")

Admin 2 Page

![Admin 2 Page](img/admin2.png "Admin 2 Page")

Admin 3 Page

![Admin 3 Page](img/admin3.png "Admin 3 Page")