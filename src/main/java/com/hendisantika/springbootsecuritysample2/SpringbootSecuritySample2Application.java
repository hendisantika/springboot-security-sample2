package com.hendisantika.springbootsecuritysample2;

import com.hendisantika.springbootsecuritysample2.entity.Role;
import com.hendisantika.springbootsecuritysample2.entity.User;
import com.hendisantika.springbootsecuritysample2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringbootSecuritySample2Application {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecuritySample2Application.class, args);
    }

    @PostConstruct
    public void init() {
        User user = new User();
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("123456"));
        user.setEnabled(true);
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }
}

