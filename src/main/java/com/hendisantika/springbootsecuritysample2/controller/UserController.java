package com.hendisantika.springbootsecuritysample2.controller;

import com.hendisantika.springbootsecuritysample2.entity.User;
import com.hendisantika.springbootsecuritysample2.service.UserJoinService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/09/18
 * Time: 11.52
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Controller
public class UserController {

    @Autowired
    UserJoinService userJoinService;


    @GetMapping("/user")
    public String users(Principal principal) {
        log.debug("principal={}", principal);
        return "user";
    }

    @GetMapping("/login")
    public String loginForm(Model model, String status) {
        model.addAttribute("status", status);
        return "login";
    }

    @GetMapping("/join")
    public String joinForm() {
        return "join";
    }

    @PostMapping("/join")
    public String join(User user) {
        userJoinService.join(user);
        return "redirect:/login?status=joinSuccess";
    }
}
