package com.hendisantika.springbootsecuritysample2.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/09/18
 * Time: 11.45
 * To change this template use File | Settings | File Templates.
 */
@Data
@Entity
public class User {
    @Id
    @Column(length = 32)
    private String username;

    @Column(nullable = false)
    private String password;

    private boolean enabled;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private Role role;
}
