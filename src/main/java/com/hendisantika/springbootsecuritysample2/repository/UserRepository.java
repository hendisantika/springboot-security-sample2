package com.hendisantika.springbootsecuritysample2.repository;

import com.hendisantika.springbootsecuritysample2.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/09/18
 * Time: 11.46
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
}