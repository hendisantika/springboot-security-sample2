package com.hendisantika.springbootsecuritysample2.service;

import com.hendisantika.springbootsecuritysample2.entity.Role;
import com.hendisantika.springbootsecuritysample2.entity.User;
import com.hendisantika.springbootsecuritysample2.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-sample2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/09/18
 * Time: 11.50
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Service
public class UserJoinService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepository userRepository;

    public User join(User user) {

        if (userRepository.findByUsername(user.getUsername()) != null) {
            return null;
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setRole(Role.USER);

        User newUser = userRepository.save(user);
        log.debug("newUser={}", newUser);

        return newUser;
    }
}